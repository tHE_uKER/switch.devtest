﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;

        public DateTime TransactionDate
        {
            get;
        }

        public Transaction(double amount)
        {
            this.amount = amount;
            TransactionDate = DateProvider.Now();
        }
    }
}

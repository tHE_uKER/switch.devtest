﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpBank
{
    public class Account
    {
        public enum AcctType
        {
            Checking,
            Savings,
            MaxiSavings,
            MaxiSavingsNew
        }
        public AcctType AccountType { get; }

        public string AccountTypeName()
        {
            string RetVal = "";

            switch (AccountType)
            {
                case AcctType.Checking:
                    RetVal += "Checking Account";
                    break;
                case AcctType.Savings:
                    RetVal += "Savings Account";
                    break;
                case AcctType.MaxiSavings:
                    RetVal += "Maxi Savings Account";
                    break;
                case AcctType.MaxiSavingsNew:
                    RetVal += "Maxi Savings Account (new)";
                    break;
                default:
                    throw new ArgumentException("invalid accountType");
            }

            return RetVal;
        }

        private List<Transaction> transactions;

        public Account(AcctType accountType)
        {
            this.AccountType = accountType;
            this.transactions = new List<Transaction>();
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else if (amount > GetBalance())
            {
                throw new InvalidOperationException("insufficient funds");
            }
            else
            {
                transactions.Add(new Transaction(-amount));
            }
        }

        public double GetInterestEarned()
        {
            double RetVal = 0;
            double Amount = GetBalance();

            if (AccountType != AcctType.MaxiSavingsNew) //Range-based interest calculations
            {
                var Arr = GetInterestDefinitions(AccountType);
                var AuxAmt = Amount;
                foreach (InterestRange r in Arr)
                {
                    RetVal += r.GetInterest(AuxAmt);
                    AuxAmt -= r.RangeSize;
                    if (AuxAmt <= 0) break;
                }
            }
            else //Check recent withdrawals
            {
                var RecentWithdrawals = transactions.Where(x => x.amount < 0 && (DateProvider.Now() - x.TransactionDate).TotalDays < 10);
                if (!RecentWithdrawals.Any())
                {
                    RetVal = Amount * 5 / 100;
                }
                else
                {
                    RetVal = Amount * 0.1 / 100;
                }
            }

            return RetVal;
        }

        public List<InterestRange> GetInterestDefinitions(AcctType accountType)
        {
            var RetVal = new List<InterestRange>();
            switch (accountType)
            {
                case AcctType.Checking:
                    RetVal.AddRange(new[] { new InterestRange(0.1) });
                    break;
                case AcctType.Savings:
                    RetVal.AddRange(new[] { new InterestRange(1000, 0.1), new InterestRange(0.2) });
                    break;
                case AcctType.MaxiSavings:
                    RetVal.AddRange(new[] { new InterestRange(1000, 2), new InterestRange(1000, 5), new InterestRange(10) });
                    break;
                default:
                    throw new ArgumentException("Unhandled accountType.");
            }
            return RetVal;
        }

        public double GetBalance()
        {
            double amount = 0.0;
            foreach (Transaction t in transactions)
                amount += t.amount;

            return amount;
        }

        public string GetStatement()
        {
            StringBuilder s = new StringBuilder();

            s.Append(AccountTypeName() + "\n");

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in transactions)
            {
                s.Append("  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + Util.ToDollars(t.amount) + "\n");
                total += t.amount;
            }
            s.Append("Total " + Util.ToDollars(total));

            return s.ToString();
        }

        public void Transfer(double amount, Account destAccount)
        {
            this.Withdraw(amount);
            destAccount.Deposit(amount);
        }
    }
}

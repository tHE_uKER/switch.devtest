﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> customers;

        public Bank()
        {
            customers = new List<Customer>();
        }

        public Customer AddCustomer(string name)
        {
            Customer customer = new Customer(name);
            customers.Add(customer);
            return customer;
        }

        public string CustomerSummary()
        {
            var summary = new StringBuilder();
            summary.Append("Customer Summary");
            foreach (Customer c in customers)
                summary.Append("\n - " + c.GetName() + " (" + Format(c.GetNumberOfAccounts(), "account") + ")");

            return summary.ToString();
        }

        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        private string Format(int number, String word)
        {
            return number + " " + word + (number == 1 ? "" : "s");
        }

        public double TotalInterestPaid()
        {
            double total = 0;
            foreach (Customer c in customers)
                total += c.GetTotalInterestEarned();
            return total;
        }

        public string GetFirstCustomerName()
        {
            if (customers.Count == 0)
                throw new InvalidOperationException("No customers found.");

            return customers[0].GetName();
        }
    }
}

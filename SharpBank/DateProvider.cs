﻿using System;

namespace SharpBank
{
    public class DateProvider
    {
        static bool UseTestDate = false;
        static DateTime TestDate;

        public static void SetTestDate(DateTime date)
        {
            TestDate = date;
            UseTestDate = true;
        }

        public static DateTime Now()
        {
            DateTime RetVal;

            if (UseTestDate)
            {
                RetVal = TestDate;
                UseTestDate = false;
            } else
            {
                RetVal = DateTime.Now;
            }

            return RetVal;
        }
    }
}

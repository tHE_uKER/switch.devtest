﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpBank
{
    public class Customer
    {
        private String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public String GetName()
        {
            return name;
        }

        public Account OpenAccount(Account.AcctType accountType)
        {
            Account account=new Account(accountType);
            accounts.Add(account);
            return account;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double GetTotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in accounts)
                total += a.GetInterestEarned();
            return total;
        }

        public String GetStatement()
        {
            StringBuilder statement = new StringBuilder();
            statement.Append("Statement for " + name + "\n");
            double total = 0.0;

            foreach (Account a in accounts)
            {
                statement.Append("\n" + a.GetStatement() + "\n");
                total += a.GetBalance();
            }
            statement.Append("\nTotal In All Accounts " + Util.ToDollars(total));

            return statement.ToString();
        }      
    }
}

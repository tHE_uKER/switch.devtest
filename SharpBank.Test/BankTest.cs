﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = bank.AddCustomer("John");
            john.OpenAccount(Account.AcctType.Checking);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Customer bill = bank.AddCustomer("Bill");
            Account checkingAccount = bill.OpenAccount(Account.AcctType.Checking);
            
            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Account savingsAccount = bank.AddCustomer("Bill").OpenAccount(Account.AcctType.Savings);

            savingsAccount.Deposit(1500.0);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            Bank bank = new Bank();
            Account maxiSavingsAccount = bank.AddCustomer("Bill").OpenAccount(Account.AcctType.MaxiSavings);
             
            maxiSavingsAccount.Deposit(500); //Start with $500
            Assert.AreEqual(10.0, bank.TotalInterestPaid(), DOUBLE_DELTA);

            maxiSavingsAccount.Deposit(1000); //Now has $1500
            Assert.AreEqual(45.0, bank.TotalInterestPaid(), DOUBLE_DELTA);

            maxiSavingsAccount.Deposit(1000.0); //Now has 2500
            Assert.AreEqual(120.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountNew()
        {
            Bank bank = new Bank();
            Account maxiSavingsAccount = bank.AddCustomer("Bill").OpenAccount(Account.AcctType.MaxiSavingsNew);

            maxiSavingsAccount.Deposit(1000);
            DateProvider.SetTestDate(DateTime.Parse("1/1/2010"));
            maxiSavingsAccount.Withdraw(500); //Withdraw with fake date
            Assert.AreEqual(25, bank.TotalInterestPaid(), DOUBLE_DELTA);

            maxiSavingsAccount.Deposit(500);
            maxiSavingsAccount.Withdraw(500); //Back to $500, now with recent withdrawal

            Assert.AreEqual(0.5, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }
    }
}

﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {
            Customer henry = new Customer("Henry");
            Account checkingAccount = henry.OpenAccount(Account.AcctType.Checking);
            Account savingsAccount = henry.OpenAccount(Account.AcctType.Savings);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar");
            oscar.OpenAccount(Account.AcctType.Savings);
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar");
            oscar.OpenAccount(Account.AcctType.Savings);
            oscar.OpenAccount(Account.AcctType.Checking);
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar");
            oscar.OpenAccount(Account.AcctType.Savings);
            oscar.OpenAccount(Account.AcctType.Checking);
            oscar.OpenAccount(Account.AcctType.MaxiSavings);
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }
    }
}
